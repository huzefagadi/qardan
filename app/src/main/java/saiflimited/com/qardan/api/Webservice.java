package saiflimited.com.qardan.api;


import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import saiflimited.com.qardan.beans.Membership;
import saiflimited.com.qardan.beans.Transaction;
import saiflimited.com.qardan.beans.User;

public class Webservice {
    private static final String NAMESPACE = "http://tempuri.org/";
    private static TextView tv;
    String resultdata;
    StringBuilder sb;
    private static final String TAG = Webservice.class.getName();

    String URL = "http://qh.bqa.la/ReliableQHServices/QarzanHasanah.svc";

    public String CollectorsBalance(String pUserID) {
        String SOAP_ACTION = "http://tempuri.org/IEnquiries_QH/MobileCollectorsBalance";
        String METHOD_NAME = "MobileCollectorsBalance";
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pUserID");
        propertyInfo.setValue(pUserID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            this.resultdata = androidHttpTransport.responseDump;
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
        }
        return this.resultdata.toString();
    }

    public String sendFeedback(String pMemberID, String pSubject, String pMessage) {
        String SOAP_ACTION = "http://tempuri.org/ISavingMembers/MembersFeedBack";
        String METHOD_NAME = "MembersFeedBack";
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pMemberID");
        propertyInfo.setValue(pMemberID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pSubject");
        propertyInfo.setValue(pSubject);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pMessage");
        propertyInfo.setValue(pMessage);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapPrimitive soapPrimitive = (SoapPrimitive) envelope.getResponse();
            return soapPrimitive.getValue().toString();
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
            return exception.getLocalizedMessage();
        }
        
    }

    public String changePassword(String pITSID, String pOldPassword, String pNewPassword) {
        String SOAP_ACTION = "http://tempuri.org/ISavingMembers/MemberChangePassword";
        String METHOD_NAME = "MemberChangePassword";
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pITSID");
        propertyInfo.setValue(pITSID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pOldPassword");
        propertyInfo.setValue(pOldPassword);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pNewPassword");
        propertyInfo.setValue(pNewPassword);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapPrimitive soapPrimitive = (SoapPrimitive) envelope.getResponse();
            return soapPrimitive.getValue().toString();
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
            return exception.getLocalizedMessage();
        }
    }

    public User userLogin(String pITSID, String pPassword) {
        User user = new User();
        String SOAP_ACTION = "http://tempuri.org/ISavingMembers/MemberLogin";
        String METHOD_NAME = "MemberLogin";
        Type DATA_TYPE = new TypeToken<HashMap<String, String>>() {
        }.getType();
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pITSID");
        propertyInfo.setValue(pITSID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pPassword");
        propertyInfo.setValue(pPassword);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject soapObject = (SoapObject) envelope.getResponse();
            Map<String, String> hashMap = new HashMap<>();
            for (int i = 0; i < soapObject.getPropertyCount(); i++) {
                hashMap.put(soapObject.getPropertyInfo(i).name, String.valueOf(soapObject.getProperty(i)));
            }
            String json = new Gson().toJson(hashMap, DATA_TYPE);
            Log.d(TAG, json);
            user = new Gson().fromJson(new Gson().toJson(hashMap, DATA_TYPE), User.class);
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
        }
        return user;
    }

    public List<Membership> getQardanAccByITSID(String pOrganizationID, String pITSID) {
        List<Membership> memberships = new ArrayList<>();
        String SOAP_ACTION = "http://tempuri.org/ISavingMembers/QarzAccountbyITSID";
        String METHOD_NAME = "QarzAccountbyITSID";
        Type DATA_TYPE = new TypeToken<HashMap<String, String>>() {
        }.getType();
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pOrganizationID");
        propertyInfo.setValue(pOrganizationID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pITSID");
        propertyInfo.setValue(pITSID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject soapObject = (SoapObject) envelope.getResponse();
            if (soapObject != null) {
                SoapObject properties = (SoapObject) ((SoapObject) soapObject.getProperty(1)).getProperty(0);
                for (int j = 0; j < properties.getPropertyCount(); j++) {
                    SoapObject property = (SoapObject) properties.getProperty(j);
                    Map<String, String> hashMap = new HashMap<>();
                    for (int i = 0; i < property.getPropertyCount(); i++) {
                        hashMap.put(property.getPropertyInfo(i).name, String.valueOf(property.getProperty(i)));
                    }
                    String json = new Gson().toJson(hashMap, DATA_TYPE);
                    Log.d(TAG, json);
                    Membership membership = new Gson().fromJson(new Gson().toJson(hashMap, DATA_TYPE), Membership.class);
                    memberships.add(membership);
                }
            }
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
            return null;
        }
        return memberships;
    }

    public List<Transaction> getMemberMobileStatement(String pMemberID) {
        List<Transaction> transactions = new ArrayList<>();
        Type DATA_TYPE = new TypeToken<HashMap<String, String>>() {
        }.getType();

        String SOAP_ACTION = "http://tempuri.org/IEnquiries_QH/MemberMobileStatement";
        String METHOD_NAME = "MemberMobileStatement";
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pMemberID");
        propertyInfo.setValue(pMemberID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject soapObject = (SoapObject) envelope.getResponse();
            if (soapObject != null) {
                SoapObject properties = (SoapObject) ((SoapObject) soapObject.getProperty(1)).getProperty(0);
                for (int j = 0; j < properties.getPropertyCount(); j++) {
                    SoapObject property = (SoapObject) properties.getProperty(j);
                    Map<String, String> hashMap = new HashMap<>();
                    for (int i = 0; i < property.getPropertyCount(); i++) {
                        hashMap.put(property.getPropertyInfo(i).name, String.valueOf(property.getProperty(i)));
                    }
                    String json = new Gson().toJson(hashMap, DATA_TYPE);
                    Log.d(TAG, json);
                    Transaction transaction = new Gson().fromJson(new Gson().toJson(hashMap, DATA_TYPE), Transaction.class);
                    transactions.add(transaction);
                }
            }
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
        }
        return transactions;
    }

    public String MemberMobileStatementDetails(String pTransactionID) {
        //String URL = "http://37.34.179.246/ReliableQHServices/QarzanHasanah.svc";
        String SOAP_ACTION = "http://tempuri.org/IEnquiries_QH/MemberMobileStatementDetails";
        String METHOD_NAME = "MemberMobileStatementDetails";
        SoapObject request = new SoapObject(NAMESPACE, "MemberMobileStatementDetails");
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pTransactionID");
        propertyInfo.setValue(pTransactionID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call("http://tempuri.org/IEnquiries_QH/MemberMobileStatementDetails", envelope);
            this.resultdata = androidHttpTransport.responseDump;
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
        }
        return this.resultdata.toString();
    }

    public String getMembersCurrentBalance(String pMemberID) {
       // String URL = "http://37.34.179.246/ReliableQHServices/QarzanHasanah.svc";
        String SOAP_ACTION = "http://tempuri.org/ISavingMembers/MembersCurrentBalance";
        String METHOD_NAME = "MembersCurrentBalance";
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo = new PropertyInfo();
        propertyInfo.setName("pMemberID");
        propertyInfo.setValue(pMemberID);
        propertyInfo.setType(Integer.class);
        request.addProperty(propertyInfo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        Log.e(TAG, " = " + request);
        envelope.setOutputSoapObject(request);
        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapPrimitive soapPrimitive = (SoapPrimitive) envelope.getResponse();
            return soapPrimitive.getValue().toString();
        } catch (Exception exception) {
            Log.e(TAG,  exception.getLocalizedMessage());
            exception.printStackTrace();
        }
        return null;
    }
}
