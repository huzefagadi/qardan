package saiflimited.com.qardan.api;


import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import saiflimited.com.qardan.model.ApiResponse;
import saiflimited.com.qardan.model.StartStop;
import saiflimited.com.qardan.model.ThaaliFeedback;

public interface RestfulApi {

    String URL = "https://us-central1-qarzan-a0e3a.cloudfunctions.net/app/";

    @POST("thali-feedback")
    Observable<ApiResponse> sendFeedback(@Body ThaaliFeedback thaaliFeedback);

    @POST("thali-start-stop")
    Observable<ApiResponse> sendStartStopRequest(@Body StartStop startStop);
}