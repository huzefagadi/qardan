package saiflimited.com.qardan.db;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.greenrobot.greendao.annotation.Generated;

@Entity

public class Message {
    @Id(autoincrement = true)
    public Long id;

    @Property(nameInDb = "message")
    public String message;

    @Property(nameInDb = "title")
    public String title;

    @Property(nameInDb = "timestamp")
    public Date timestamp;

    public String formattedTimeStamp() {
        if (timestamp != null) {
            return new SimpleDateFormat("MM-dd-YYYY HH:mm:ss").format(timestamp);
        } else {
            return "";
        }

    }

    @Generated(hash = 753013436)
    public Message(Long id, String message, String title, Date timestamp) {
        this.id = id;
        this.message = message;
        this.title = title;
        this.timestamp = timestamp;
    }

    @Generated(hash = 637306882)
    public Message() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}