package saiflimited.com.qardan.db;

import android.app.Application;

import java.util.List;

import saiflimited.com.qardan.MyApplication;

public class Repository {
    DaoSession daoSession;

    public Repository(Application application) {
        daoSession = ((MyApplication) application).getDaoSession();
    }

    public List<Message> getAllMessages() {
        return daoSession.getMessageDao().loadAll();
    }

    public void saveMessage(Message message) {
        daoSession.insertOrReplace(message);
    }
}
