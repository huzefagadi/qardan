package saiflimited.com.qardan.repo;

import io.reactivex.Observable;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.api.Webservice;

public class LoginRepository {
    private static final String TAG = LoginRepository.class.getName();
    Webservice webservice = new Webservice();

    public Observable<User> loginUser(String username, String password) {
        return Observable.fromCallable(() -> webservice.userLogin(username, password));
    }
}
