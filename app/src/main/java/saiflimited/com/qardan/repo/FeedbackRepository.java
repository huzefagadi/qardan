package saiflimited.com.qardan.repo;

import android.content.Context;

import io.reactivex.Observable;
import saiflimited.com.qardan.MyApplication;
import saiflimited.com.qardan.api.RestfulApi;
import saiflimited.com.qardan.api.Webservice;
import saiflimited.com.qardan.model.ApiResponse;
import saiflimited.com.qardan.model.StartStop;
import saiflimited.com.qardan.model.ThaaliFeedback;

public class FeedbackRepository {
    Context context;
    private static final String TAG = FeedbackRepository.class.getName();
    Webservice webservice = new Webservice();
    RestfulApi restfulApi;

    public FeedbackRepository(Context context) {
        this.context = context;
        restfulApi = ((MyApplication) context.getApplicationContext()).getApiService();
    }

    public FeedbackRepository() {

    }


    public Observable<String> sendFeedback(String memberId, String subject, String feedback) {
        return Observable.fromCallable(() -> webservice.sendFeedback(memberId, subject, feedback));
    }


    public Observable<ApiResponse> sendThaaliFeedback(ThaaliFeedback thaaliFeedback) {
        return restfulApi.sendFeedback(thaaliFeedback);
    }

    public Observable<ApiResponse> sendStartStopRequest(StartStop startStop) {
        return restfulApi.sendStartStopRequest(startStop);
    }
}
