package saiflimited.com.qardan.repo;

import java.util.List;

import io.reactivex.Observable;
import saiflimited.com.qardan.api.Webservice;
import saiflimited.com.qardan.beans.Membership;

public class SummaryRepository {
    Webservice webservice = new Webservice();

    public Observable<List<Membership>> getQardanAccByITSID(String orgId, String itsId) {
        return Observable.fromCallable(() -> webservice.getQardanAccByITSID(orgId, itsId));
    }

    public Observable<String> getMembersCurrentBalance(String memberId) {
        return Observable.fromCallable(() -> webservice.getMembersCurrentBalance(memberId));
    }
}
