package saiflimited.com.qardan.repo;


import io.reactivex.Observable;
import saiflimited.com.qardan.api.Webservice;

public class PasswordRepository {
    private static final String TAG = PasswordRepository.class.getName();
    Webservice webservice = new Webservice();

    public Observable<String> changePassword(String itsId, String oldPassword, String newPassword) {
        return Observable.fromCallable(() -> webservice.changePassword(itsId, oldPassword , newPassword));
    }
}

