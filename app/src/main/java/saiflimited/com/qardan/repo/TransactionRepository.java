package saiflimited.com.qardan.repo;

import java.util.List;

import io.reactivex.Observable;
import saiflimited.com.qardan.api.Webservice;
import saiflimited.com.qardan.beans.Transaction;

public class TransactionRepository {

    Webservice webservice = new Webservice();

    public Observable<List<Transaction>> getMemberMobileStatement(String memberId) {
        return Observable.fromCallable(() -> webservice.getMemberMobileStatement(memberId));
    }
}
