package saiflimited.com.qardan.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import saiflimited.com.qardan.BR;

public class LoginModel extends BaseObservable {
    private String its;
    private String password;

    @Bindable
    public String getIts() {
        return this.its;
    }

    @Bindable
    public String getPassword() {
        return this.password;
    }

    public void setIts(String its) {
        this.its = its;
        notifyPropertyChanged(BR.its);
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }
}
