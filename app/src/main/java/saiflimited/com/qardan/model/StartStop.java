package saiflimited.com.qardan.model;

import com.google.gson.annotations.SerializedName;

public class StartStop {

    private String name;
    private String sabeel;
    private String date;
    @SerializedName("startStop")
    private String requestType;
    @SerializedName("remarks")
    private String remarks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSabeel() {
        return sabeel;
    }

    public void setSabeel(String sabeel) {
        this.sabeel = sabeel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
