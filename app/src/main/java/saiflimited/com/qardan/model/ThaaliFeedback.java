package saiflimited.com.qardan.model;

import com.google.gson.annotations.SerializedName;

public class ThaaliFeedback {
    private String name;
    private String sabeel;
    private String date;
    @SerializedName("mainTarkari")
    private String mainTarkariRating;
    @SerializedName("roti")
    private String rotiRating;
    @SerializedName("rice")
    private String riceRating;
    @SerializedName("soupGravy")
    private String soupRating;
    @SerializedName("other")
    private String remarks;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMainTarkariRating() {
        return mainTarkariRating;
    }

    public void setMainTarkariRating(String mainTarkariRating) {
        this.mainTarkariRating = mainTarkariRating;
    }

    public String getRotiRating() {
        return rotiRating;
    }

    public void setRotiRating(String rotiRating) {
        this.rotiRating = rotiRating;
    }

    public String getRiceRating() {
        return riceRating;
    }

    public void setRiceRating(String riceRating) {
        this.riceRating = riceRating;
    }

    public String getSoupRating() {
        return soupRating;
    }

    public void setSoupRating(String soupRating) {
        this.soupRating = soupRating;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSabeel() {
        return sabeel;
    }

    public void setSabeel(String sabeel) {
        this.sabeel = sabeel;
    }
}
