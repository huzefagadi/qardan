package saiflimited.com.qardan;

public class Constants {
    public static final String INTENT_USER = "user";
    public static final String INTENT_MEMBER = "member";
    public static final String INTENT_TRANSACTION = "transaction";
    public static final String TOPIC_COMMON_FOR_ALL = "all";
    public static final String INTENT_MESSAGE = "message";
}
