package saiflimited.com.qardan.beans;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("MemberID")
    public String memberId;
    @SerializedName("OrganizationID")
    public String organizationId;
    @SerializedName("MembershipNo")
    public String membershipNo;
    @SerializedName("MemberName")
    public String memberName;
    @SerializedName("SabeelNo")
    public String sabeelNo;
    @SerializedName("ITSNo")
    public String itsNo;
    @SerializedName("Address")
    public String address;
    @SerializedName("Mobile")
    public String mobileNo;
    @SerializedName("Email")
    public String email;
    @SerializedName("OpeningDate")
    public String openingDate;
    @SerializedName("OpeningBalance")
    public Double openingBal;
    @SerializedName("CurrentBalance")
    public Double currentBal;
    @SerializedName("BlockedAmount")
    public Double blockedAmt;
    @SerializedName("CreatedOn")
    public String createdOn;
    @SerializedName("CreatedBy")
    public String createdBy;
    @SerializedName("ModifiedOn")
    public String modifiedOn;
    @SerializedName("ModifiedBy")
    public String modifiedBy;
    @SerializedName("isLoginSuccess")
    public boolean isLoginSuccess;
    @SerializedName("HOFID")
    public String hofID;
    @SerializedName("isInternalAccount")
    public boolean isInternalAcc;
}
