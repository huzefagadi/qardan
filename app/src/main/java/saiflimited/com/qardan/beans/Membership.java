package saiflimited.com.qardan.beans;

import com.google.gson.annotations.SerializedName;

public class Membership {
    @SerializedName("MemberID")
    public String memberId;
    @SerializedName("MembershipNo")
    public String membershipNo;
    @SerializedName("MemberName")
    public String memberName;
    @SerializedName("ITSNumber")
    public String its;
    public String currentBalance;
}
