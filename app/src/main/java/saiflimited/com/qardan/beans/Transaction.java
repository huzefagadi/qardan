package saiflimited.com.qardan.beans;

import com.google.gson.annotations.SerializedName;

public class Transaction {

    public static final String CREDIT = "A";
    public static final String DEBIT = "C";

    @SerializedName("RecOrder")
    public String txnType;
    @SerializedName("Name")
    public String name;
    @SerializedName("TelResi")
    public String telResident;
    @SerializedName("Mobile")
    public String mobile;
    @SerializedName("TrnxDate")
    public String txnDate;
    @SerializedName("ReceiptNo")
    public String recieptNo;
    @SerializedName("Description")
    public String description;
    @SerializedName("Debit")
    public double debit;
    @SerializedName("Credit")
    public double credit;
    @SerializedName("BlockedAmount")
    public double blockedAmt;
    @SerializedName("Balance")
    public double balance;
    @SerializedName("CreatedOn")
    public String createdOn;
    @SerializedName("TrnxID")
    public String txnID;
    @SerializedName("RecieptDate")
    public String recieptDate;

}
