package saiflimited.com.qardan;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.multidex.MultiDex;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.annotations.NonNull;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import saiflimited.com.qardan.api.RestfulApi;
import saiflimited.com.qardan.db.DaoMaster;
import saiflimited.com.qardan.db.DaoSession;
import saiflimited.com.qardan.db.DbOpenHelper;


public class MyApplication extends Application {
    private DaoSession mDaoSession;

    public static final int DISK_CACHE_SIZE = 10 * 1024 * 1024; // 10 MB

    private RestfulApi apiService;
    private InternetConnectionListener mInternetConnectionListener;


    @Override
    public void onCreate() {
        super.onCreate();
        mDaoSession = new DaoMaster(new DbOpenHelper(this, "poseidon.db").getWritableDb()).newSession();
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }


    public void setInternetConnectionListener(InternetConnectionListener listener) {
        mInternetConnectionListener = listener;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void removeInternetConnectionListener() {
        mInternetConnectionListener = null;
    }

    public RestfulApi getApiService() {
        if (apiService == null) {
            apiService = provideRetrofit(RestfulApi.URL).create(RestfulApi.class);
        }
        return apiService;
    }

    private boolean isInternetAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private Retrofit provideRetrofit(String url) {
        return new Retrofit.Builder()
                .baseUrl(url)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        HeaderInterceptor headerInterceptor = new HeaderInterceptor(getApplicationContext());
        okHttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        okHttpClientBuilder.cache(getCache());
        okHttpClientBuilder.addInterceptor(loggingInterceptor);
        okHttpClientBuilder.addInterceptor(headerInterceptor);
        okHttpClientBuilder.addInterceptor(new NetworkConnectionInterceptor() {
            @Override
            public boolean isInternetAvailable() {
                return MyApplication.this.isInternetAvailable();
            }

            @Override
            public void onInternetUnavailable() {
                if (mInternetConnectionListener != null) {
                    mInternetConnectionListener.onInternetUnavailable();
                }
            }

            @Override
            public void onCacheUnavailable() {
                if (mInternetConnectionListener != null) {
                    mInternetConnectionListener.onCacheUnavailable();
                }
            }
        });

        return okHttpClientBuilder.build();
    }

    public Cache getCache() {
        File cacheDir = new File(getCacheDir(), "cache");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        return cache;
    }

    class HeaderInterceptor implements Interceptor {

        final Context context;

        HeaderInterceptor(Context context) {
            this.context = context;
        }

        @Override
        public Response intercept(@NonNull Interceptor.Chain chain)
                throws IOException {
            Request request = chain.request();
//            String token = SharedPrefUtil.getToken(context);
//            long expiry = SharedPrefUtil.getExpirationTimeInMillis(context);
//            if (isInternetAvailable() && Calendar.getInstance().getTimeInMillis() + (5 * 60 * 1000) >= expiry) {
//                try {
//                    Task<GetTokenResult> task = Objects.requireNonNull(FirebaseAuth.getInstance()
//                            .getCurrentUser())
//                            .getIdToken(true);
//                    GetTokenResult tokenResult = Tasks.await(task);
//                    if (task.isSuccessful()) {
//                        token = tokenResult.getToken();
//                        SharedPrefUtil.saveToken(context, token);
//                        SharedPrefUtil.saveExpirationTimeInMillis(context,
//                                tokenResult.getExpirationTimestamp());
//                        request = request.newBuilder()
//                                .addHeader("Authorization", "Bearer " + token)
//                                .build();
//                    }
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                request = request.newBuilder()
//                        .addHeader("Authorization", "Bearer " + token)
//                        .build();
//            }
            return chain.proceed(request);
        }
    }

    public abstract class NetworkConnectionInterceptor implements Interceptor {

        public abstract boolean isInternetAvailable();

        public abstract void onInternetUnavailable();

        public abstract void onCacheUnavailable();

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            if (!isInternetAvailable()) {
                onInternetUnavailable();
                request = request.newBuilder().header("Cache-Control",
                        "public, only-if-cached, max-stale=" + 60 * 60 * 24).build();
                Response response = chain.proceed(request);
                if (response.cacheResponse() == null) {
                    onCacheUnavailable();
                }
                return response;
            }
            return chain.proceed(request);
        }
    }

    public interface InternetConnectionListener {
        void onInternetUnavailable();

        void onCacheUnavailable();
    }
}

