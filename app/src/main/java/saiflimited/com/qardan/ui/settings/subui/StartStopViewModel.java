package saiflimited.com.qardan.ui.settings.subui;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.model.StartStop;
import saiflimited.com.qardan.repo.FeedbackRepository;

public class StartStopViewModel extends ViewModel {

    User user;
    Context mContext;
    DatePickerDialog datePickerDialog;
    FeedbackRepository repository;
    private SimpleDateFormat sd = new SimpleDateFormat("dd MMM yyyy");
    public MutableLiveData<Integer> startOrStop = new MutableLiveData<>();
    private MutableLiveData<Boolean> showLoader = new MutableLiveData<>();
    public MutableLiveData<String> feedback = new MutableLiveData<>();
    public MutableLiveData<String> date = new MutableLiveData<>();

    public void setUser(User user) {
        this.user = user;
    }

    public MutableLiveData<Boolean> getShowLoader() {
        return showLoader;
    }

    Map<Integer, Integer> map = new HashMap<>();

    void init(Context context) {
        mContext = context;
        repository = new FeedbackRepository(mContext);
        date.setValue(sd.format(new Date()));
        map.put(R.id.start, R.string.start);
        map.put(R.id.stop, R.string.stop);
        startOrStop.setValue(R.id.start);
        initDateTimePicker();
    }

    private void initDateTimePicker() {
        final Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(mContext, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            date.setValue(sd.format(new Date(newDate.getTimeInMillis())));
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DAY_OF_YEAR, 1);

        datePickerDialog.getDatePicker().setMinDate(tomorrow.getTimeInMillis());
    }

    @SuppressLint("CheckResult")
    public void onButtonClick() {
        askForConfirmationBeforeSubmitting();
    }

    private void askForConfirmationBeforeSubmitting() {
        new MaterialDialog.Builder(mContext)
                .title("Please confirm")
                .content("Do you want to " + mContext.getString(map.get(startOrStop.getValue())) + " the Thali from " + date.getValue() + " ?")
                .positiveText("Yes")
                .negativeText("No")
                .onPositive((dialog1, which) -> sendResponseToServer())
                .onNegative((dialog, which) -> dialog.dismiss())
                .build().show();
    }

    @SuppressLint("CheckResult")
    private void sendResponseToServer() {
        this.showLoader.setValue(true);
        StartStop startStop = new StartStop();
        startStop.setName(user.memberName);
        startStop.setSabeel(user.sabeelNo);
        try {
            startStop.setDate(sd.parse(date.getValue()).toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        startStop.setRemarks(feedback.getValue() != null ? feedback.getValue() : "");
        startStop.setRequestType(mContext.getString(map.get(startOrStop.getValue())));
        repository.sendStartStopRequest(startStop)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    showLoader.setValue(false);
                    Log.d("Response", s.getData());
                    Toast.makeText(mContext, s.getData(), Toast.LENGTH_LONG).show();
                    ((StartStopActivity) mContext).finish();
                }, throwable -> {
                    showLoader.setValue(false);
                    Log.d("Error", throwable.getLocalizedMessage());
                    Toast.makeText(mContext, throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                });
    }


    public void showDatePicker() {
        datePickerDialog.show();
    }
}
