package saiflimited.com.qardan.ui.home.subui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.Membership;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.SummaryFragmentBinding;
import saiflimited.com.qardan.ui.home.HomeViewModel;
import saiflimited.com.qardan.ui.home.subui.adapter.SummaryRecyclerViewAdapter;
import saiflimited.com.qardan.utils.EqualSpacingItemDecoration;

public class SummaryFragment extends Fragment implements SummaryRecyclerViewAdapter.SummaryRecyclerViewAdapterListener {

    private SummaryViewModel mViewModel;
    private HomeViewModel mActivityViewModel;
    private SummaryFragmentBinding summaryFragmentBinding;
    private User mUser;
    RecyclerView mRecyclerView;

    public static SummaryFragment newInstance(String user) {
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        SummaryFragment summaryFragment = new SummaryFragment();
        summaryFragment.setArguments(bundle);
        return summaryFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        summaryFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.summary_fragment, container, false);
        return summaryFragmentBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(SummaryViewModel.class);
        mActivityViewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);
        mActivityViewModel.currentTitle.setValue("Accounts");
        mViewModel.init();
        summaryFragmentBinding.setViewModel(mViewModel);
        mUser = new Gson().fromJson(getArguments().getString(Constants.INTENT_USER), User.class);

        initRecyclerView();
        mActivityViewModel.showLoader.setValue(true);
        mViewModel.getQardanAccByITSID(mUser.organizationId, mUser.itsNo);
        mViewModel.membersList.observe(this, memberships -> {
            mActivityViewModel.showLoader.setValue(false);
            if(memberships!=null) {
                ((SummaryRecyclerViewAdapter)mRecyclerView.getAdapter()).loadMembers(memberships);
            }

        });
        mViewModel.membersCurrentBalance.observe(this, integerStringHashMap -> {
            if(integerStringHashMap!=null){
                ((SummaryRecyclerViewAdapter)mRecyclerView.getAdapter()).loadMemberBalance(integerStringHashMap);
            }
        });
    }

    private void initRecyclerView() {
        mRecyclerView = summaryFragmentBinding.recyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(mRecyclerView.getContext(),
                16, EqualSpacingItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(new SummaryRecyclerViewAdapter(this));
    }

    @Override
    public void onMembershipClicked(Membership membership) {
        Intent intent = new Intent(getActivity(),TransactionActivity.class);
        intent.putExtra(Constants.INTENT_MEMBER, new Gson().toJson(membership));
        intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
        startActivity(intent);
    }

    @Override
    public void getCurrentBalance(int index) {
        mViewModel.getMembersCurrentBalance(index);
    }


}
