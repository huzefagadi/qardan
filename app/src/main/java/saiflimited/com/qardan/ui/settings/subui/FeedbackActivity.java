package saiflimited.com.qardan.ui.settings.subui;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import java.util.Objects;

import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.FeedbackActivityBinding;

public class FeedbackActivity extends AppCompatActivity {

    MaterialDialog dialog;
    FeedbackActivityBinding binding;
    FeedbackViewModel mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding = DataBindingUtil.setContentView(this, R.layout.feedback_activity);
        binding.setLifecycleOwner(this);
        dialog = new MaterialDialog.Builder(this)
                .progress(true, 100)
                .title("Please wait..")
                .content("Sending Feedback..")
                .build();
        mViewModel = ViewModelProviders.of(this).get(FeedbackViewModel.class);
        mViewModel.setUser(new Gson().fromJson(getIntent().getStringExtra(Constants.INTENT_USER), User.class));
        binding.setViewModel(mViewModel);
        mViewModel.getShowLoader().observe(this, aBoolean -> {
            if (aBoolean) {
                dialog.show();
            } else {
                dialog.dismiss();
            }
        });

        mViewModel.getMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(FeedbackActivity.this, s, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
