package saiflimited.com.qardan.ui.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.LoginFragmentBinding;
import saiflimited.com.qardan.model.LoginModel;
import saiflimited.com.qardan.repo.LoginRepository;
import saiflimited.com.qardan.ui.home.HomeActivity;
import saiflimited.com.qardan.ui.settings.subui.ForgotPasswordActivity;

public class LoginFragment extends Fragment {

    LoginFragmentBinding loginFragmentBinding;
    private LoginViewModel mViewModel;
    private LoginRepository loginRepository = new LoginRepository();
    private static final String TAG = LoginFragment.class.getName();
    private MaterialDialog dialog;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false);
        return loginFragmentBinding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dialog = new MaterialDialog.Builder(getContext())
                .progress(true,100)
                .title("Please wait..")
                .content("Logging in..")
                .build();
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mViewModel.init();
        loginFragmentBinding.setViewModel(mViewModel);
        mViewModel.getButtonClick().observe(this, new Observer<LoginModel>() {

            /**
             * Called when the data is changed.
             *
             * @param loginModel The new data
             */
            @SuppressLint("CheckResult")
            @Override
            public void onChanged(@Nullable LoginModel loginModel) {
                mViewModel.getShowLoadingIcon().setValue(true);
                mViewModel.loginUser().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(result -> {
                            mViewModel.getShowLoadingIcon().setValue(false);
                            if(result.isLoginSuccess) {
                                subscribeToMessaging(result);
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(result));
                                getActivity().startActivity(intent);
                                getActivity().finish();
                            } else {
                                Toast.makeText(getContext(),"Login Failed!!",Toast.LENGTH_LONG).show();
                            }

                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                mViewModel.getShowLoadingIcon().setValue(false);
                                Toast.makeText(getContext(),throwable.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });

        mViewModel.getShowLoadingIcon().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean) {
                    dialog.show();
                } else {
                    dialog.dismiss();
                }

            }
        });

        mViewModel.getForgotPasswordClicked().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean) {
                    Intent intent = new Intent(getActivity(), ForgotPasswordActivity.class);
                    startActivity(intent);
                }

            }
        });
    }

    private void subscribeToMessaging(User user) {
        FirebaseMessaging.getInstance().subscribeToTopic(user.sabeelNo+"_"+user.itsNo)
                .addOnCompleteListener(task -> {

                });
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_COMMON_FOR_ALL)
                .addOnCompleteListener(task -> {

                });
    }

}
