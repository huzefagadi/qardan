package saiflimited.com.qardan.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.HomeActivityBinding;
import saiflimited.com.qardan.ui.home.subui.SummaryFragment;
import saiflimited.com.qardan.ui.login.LoginActivity;
import saiflimited.com.qardan.ui.messages.MessagesFragment;
import saiflimited.com.qardan.ui.settings.SettingsFragment;

public class HomeActivity extends AppCompatActivity {


    User mUser;
    HomeViewModel mHomeViewModel;

    @BindView(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;
    
    HomeActivityBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.home_activity);
        ButterKnife.bind(this);
        mUser = new Gson().fromJson(getIntent().getStringExtra(Constants.INTENT_USER), User.class);
        mHomeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        binding.setViewModel(mHomeViewModel);
        binding.setLifecycleOwner(this);

        mHomeViewModel.setUser(mUser);
        mHomeViewModel.init();
        mHomeViewModel.sendUserInfoToFirestore();
        setupBottomNav();
        mHomeViewModel.currentFragment.observe(this, new Observer<Fragment>() {
            @Override
            public void onChanged(Fragment fragment) {
                loadFragment(fragment);
            }
        });

        mHomeViewModel.currentTitle.observe(this, s -> setTitle(s));
        mHomeViewModel.currentTitle.setValue("Accounts");
    }


    private void setupBottomNav() {
        bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.title_home),
                R.drawable.ic_home_accent_24dp));
        bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.title_messages),
                R.drawable.ic_announcement_accent_24dp));
        bottomNavigation.addItem(new AHBottomNavigationItem(getString(R.string.title_settings),
                R.drawable.ic_settings_black_24dp));
        bottomNavigation.setDefaultBackgroundColor(ContextCompat.getColor(this, R.color.primary_text_color));
        bottomNavigation.setAccentColor(ContextCompat.getColor(this, R.color.colorPrimary));
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.setOnTabSelectedListener((position, wasSelected) -> {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = SummaryFragment.newInstance(new Gson().toJson(mUser));
                    break;
                case 1:
                    fragment = MessagesFragment.newInstance(new Gson().toJson(mUser));
                    break;
                case 2:
                    fragment = SettingsFragment.newInstance(new Gson().toJson(mUser));
                    break;
            }
            loadFragment(fragment);
            return true;
        });
        bottomNavigation.setCurrentItem(0);
    }

    void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.mybutton) {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
