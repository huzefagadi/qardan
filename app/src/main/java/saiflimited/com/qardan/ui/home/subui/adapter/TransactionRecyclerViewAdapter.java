package saiflimited.com.qardan.ui.home.subui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.Transaction;
import saiflimited.com.qardan.databinding.TransactionRowItemBinding;


public class TransactionRecyclerViewAdapter extends RecyclerView.Adapter<TransactionRecyclerViewAdapter.MyViewHolder> {

    private List<Transaction> transactionList;
    private LayoutInflater layoutInflater;
    private TransactionRecyclerViewAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TransactionRowItemBinding binding;

        public MyViewHolder(final TransactionRowItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public void loadTransactions(List<Transaction> transactionList) {
        this.transactionList = transactionList;
        notifyDataSetChanged();
    }


    public TransactionRecyclerViewAdapter(TransactionRecyclerViewAdapterListener listener) {
        this.transactionList = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        TransactionRowItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.transaction_row_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.binding.setTransaction(transactionList.get(position));
        holder.itemView.setOnClickListener(v -> listener.onTransactionClicked(transactionList.get(position)));
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    public interface TransactionRecyclerViewAdapterListener {
        void onTransactionClicked(Transaction transaction);
    }
}