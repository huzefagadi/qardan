package saiflimited.com.qardan.ui.settings.subui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import saiflimited.com.qardan.beans.User;

public class ProfileViewModel extends ViewModel {
    public User getUser() {
        return userMutableLiveData.getValue();
    }

    private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();

    public void setUser(User user) {
        userMutableLiveData.setValue(user);
    }


}
