package saiflimited.com.qardan.ui.login;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import saiflimited.com.qardan.model.LoginModel;
import saiflimited.com.qardan.repo.LoginRepository;
import saiflimited.com.qardan.beans.User;

public class LoginViewModel extends ViewModel {

    LoginModel loginModel;
    LoginRepository loginRepository;
    private MutableLiveData<Boolean> showLoadingIcon = new MutableLiveData<>();
    private MutableLiveData<LoginModel> buttonClick = new MutableLiveData<>();
    private MutableLiveData<Boolean> forgotPasswordClicked = new MutableLiveData<>();

    public void init() {
        loginRepository = new LoginRepository();
        loginModel = new LoginModel();
        showLoadingIcon.setValue(false);
    }

    public LoginModel getLogin() {
        return loginModel;
    }

    public io.reactivex.Observable<User> loginUser() {
        return loginRepository.loginUser(loginModel.getIts(), loginModel.getPassword());
    }

    public MutableLiveData<LoginModel> getButtonClick() {
        return buttonClick;
    }

    public void onButtonClick() {
        buttonClick.setValue(loginModel);
    }


    public MutableLiveData<Boolean> getForgotPasswordClicked() {
        return forgotPasswordClicked;
    }

    public void forgotPasswordClicked() {
        forgotPasswordClicked.setValue(true);
    }

    public MutableLiveData<Boolean> getShowLoadingIcon() {
        return showLoadingIcon;
    }
}
