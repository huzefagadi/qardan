package saiflimited.com.qardan.ui.settings.subui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import java.util.Objects;

import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.ActivityThaaliStartStopBinding;

public class StartStopActivity extends AppCompatActivity {

    StartStopViewModel mViewModel;
    ActivityThaaliStartStopBinding binding;
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_thaali_start_stop);
        binding.setLifecycleOwner(this);
        mViewModel = ViewModelProviders.of(this).get(StartStopViewModel.class);
        mViewModel.init(this);
        binding.setViewModel(mViewModel);
        dialog = new MaterialDialog.Builder(this)
                .progress(true, 100)
                .title("Please wait..")
                .content("Sending request..")
                .build();
        mViewModel.setUser(new Gson().fromJson(getIntent().getStringExtra(Constants.INTENT_USER), User.class));
        binding.setViewModel(mViewModel);
        mViewModel.getShowLoader().observe(this, aBoolean -> {
            if (aBoolean) {
                dialog.show();
            } else {
                dialog.dismiss();
            }
        });

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
