package saiflimited.com.qardan.ui.settings;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.databinding.SettingsRowItemBinding;


public class SettingsRecyclerViewAdapter extends RecyclerView.Adapter<SettingsRecyclerViewAdapter.MyViewHolder> {

    private List<String> settings;
    private LayoutInflater layoutInflater;
    private SettingsRecyclerViewAdapterListener listener;

    public void loadSettings(List<String> settings) {
        this.settings = settings;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final SettingsRowItemBinding binding;

        public MyViewHolder(final SettingsRowItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


    public SettingsRecyclerViewAdapter(SettingsRecyclerViewAdapterListener listener) {
        this.settings = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        SettingsRowItemBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.settings_row_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.binding.setName(settings.get(position));
        holder.itemView.setOnClickListener(v -> listener.onSettingsClicked(position));
    }

    @Override
    public int getItemCount() {
        return settings.size();
    }

    public interface SettingsRecyclerViewAdapterListener {
        void onSettingsClicked(int index);
    }
}