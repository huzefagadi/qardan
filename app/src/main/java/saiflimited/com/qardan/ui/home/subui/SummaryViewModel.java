package saiflimited.com.qardan.ui.home.subui;

import android.annotation.SuppressLint;

import java.util.HashMap;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import saiflimited.com.qardan.beans.Membership;
import saiflimited.com.qardan.repo.SummaryRepository;

public class SummaryViewModel extends ViewModel {

    SummaryRepository summaryRepository;
    public MutableLiveData<List<Membership>> membersList;
    public MutableLiveData<HashMap<Integer, String>> membersCurrentBalance;

    public void init() {
        summaryRepository = new SummaryRepository();
        membersCurrentBalance = new MutableLiveData<>();
        membersList = new MutableLiveData<>();
    }

    @SuppressLint("CheckResult")
    public void getQardanAccByITSID(String orgId, String itsId) {
        summaryRepository.getQardanAccByITSID(orgId, itsId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(memberships -> {
                    membersList.setValue(memberships);
                }, throwable -> {

                });

    }

    public void getMembersCurrentBalance(int position) {
        summaryRepository.getMembersCurrentBalance(membersList.getValue().get(position).memberId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(balance -> {
                    if (membersCurrentBalance.getValue() == null) {
                        HashMap<Integer, String> map = new HashMap<>();
                        map.put(position, balance);
                        membersCurrentBalance.setValue(map);
                    } else {
                        membersCurrentBalance.getValue().put(position, balance);
                        membersCurrentBalance.setValue(membersCurrentBalance.getValue());
                    }

                }, throwable -> {
                });
    }

}
