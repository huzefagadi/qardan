package saiflimited.com.qardan.ui.settings.subui;

import android.os.Bundle;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.ChangePasswordActivityBinding;

public class ChangePasswordActivity extends AppCompatActivity {

    private ChangePasswordViewModel mViewModel;
    private ChangePasswordActivityBinding binding;
    private MaterialDialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.change_password_activity);
        binding.setLifecycleOwner(this);
        dialog = new MaterialDialog.Builder(this)
                .progress(true, 100)
                .title("Please wait..")
                .content("Changing Password..")
                .build();

        mViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        mViewModel.setUser(new Gson().fromJson(getIntent().getStringExtra(Constants.INTENT_USER), User.class));
        binding.setViewModel(mViewModel);
        mViewModel.getShowLoader().observe(this, aBoolean -> {
            if (aBoolean) {
                dialog.show();
            } else {
                dialog.dismiss();
            }
        });

        mViewModel.getMessage().observe(this, s -> {
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        });
    }


}
