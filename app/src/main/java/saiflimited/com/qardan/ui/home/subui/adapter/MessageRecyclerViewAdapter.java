package saiflimited.com.qardan.ui.home.subui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.Transaction;
import saiflimited.com.qardan.databinding.MessageRowItemBinding;
import saiflimited.com.qardan.databinding.TransactionRowItemBinding;
import saiflimited.com.qardan.db.Message;


public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<MessageRecyclerViewAdapter.MyViewHolder> {

    private List<Message> messageList;
    private LayoutInflater layoutInflater;
    private MessageRecyclerViewAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final MessageRowItemBinding binding;

        public MyViewHolder(final MessageRowItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public void loadMessages(List<Message> transactionList) {
        this.messageList = transactionList;
        notifyDataSetChanged();
    }


    public MessageRecyclerViewAdapter(MessageRecyclerViewAdapterListener listener) {
        this.messageList = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        MessageRowItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.message_row_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.binding.setMessage(messageList.get(position));
        holder.itemView.setOnClickListener(v -> listener.onMessageClicked(messageList.get(position)));
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public interface MessageRecyclerViewAdapterListener {
        void onMessageClicked(Message message);
    }
}