package saiflimited.com.qardan.ui.home.subui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.databinding.TransactionActivityBinding;

public class TransactionActivity extends AppCompatActivity {
    TransactionActivityBinding binding;
    TransactionActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.transaction_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewModel = ViewModelProviders.of(this).get(TransactionActivityViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.getCurrentFragment().observe(this, fragment -> getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack("fragment")
                .commit());

        if (savedInstanceState == null) {
            String membership = getIntent().getStringExtra(Constants.INTENT_MEMBER);
            String mUser = getIntent().getStringExtra(Constants.INTENT_USER);
            Fragment transactionFragment = TransactionsFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.INTENT_MEMBER, membership);
            bundle.putString(Constants.INTENT_USER, mUser);
            transactionFragment.setArguments(bundle);
            viewModel.getCurrentFragment().setValue(transactionFragment);
        }
    }

    @Override
    public void onBackPressed() {

        if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
