package saiflimited.com.qardan.ui.settings.subui;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.model.ThaaliFeedback;
import saiflimited.com.qardan.repo.FeedbackRepository;

public class ThaaliFeedbackViewModel extends ViewModel {

    User user;
    Context mContext;
    DatePickerDialog datePickerDialog;
    FeedbackRepository repository;
    private SimpleDateFormat sd = new SimpleDateFormat("dd MMM yyyy");
    public MutableLiveData<Integer> mainTarkariRating = new MutableLiveData<>();
    public MutableLiveData<Integer> riceRating = new MutableLiveData<>();
    public MutableLiveData<Integer> rotiRating = new MutableLiveData<>();
    public MutableLiveData<Integer> soupRating = new MutableLiveData<>();
    private MutableLiveData<Boolean> showLoader = new MutableLiveData<>();
    public MutableLiveData<String> feedback = new MutableLiveData<>();
    public MutableLiveData<String> date = new MutableLiveData<>();

    public void setUser(User user) {
        this.user = user;
    }

    public MutableLiveData<Boolean> getShowLoader() {
        return showLoader;
    }

    Map<Integer, Integer> map = new HashMap<>();

    void init(Context context) {
        mContext = context;
        repository = new FeedbackRepository(mContext);
        date.setValue(sd.format(new Date()));
        map.put(R.id.excellent, R.string.excellent);
        map.put(R.id.very_good, R.string.very_good);
        map.put(R.id.good, R.string.good);
        map.put(R.id.needs_improvement, R.string.needs_improvement);

        map.put(R.id.excellent1, R.string.excellent);
        map.put(R.id.very_good1, R.string.very_good);
        map.put(R.id.good1, R.string.good);
        map.put(R.id.needs_improvement1, R.string.needs_improvement);

        map.put(R.id.excellent2, R.string.excellent);
        map.put(R.id.very_good2, R.string.very_good);
        map.put(R.id.good2, R.string.good);
        map.put(R.id.needs_improvement2, R.string.needs_improvement);

        map.put(R.id.excellent3, R.string.excellent);
        map.put(R.id.very_good3, R.string.very_good);
        map.put(R.id.good3, R.string.good);
        map.put(R.id.needs_improvement3, R.string.needs_improvement);


        mainTarkariRating.setValue(R.id.excellent);
        riceRating.setValue(R.id.excellent1);
        rotiRating.setValue(R.id.excellent2);
        soupRating.setValue(R.id.excellent3);

        initDateTimePicker();
    }

    private void initDateTimePicker() {
        final Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(mContext, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            date.setValue(sd.format(new Date(newDate.getTimeInMillis())));
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
    }

    @SuppressLint("CheckResult")
    public void onButtonClick() {
        this.showLoader.setValue(true);
        ThaaliFeedback thaaliFeedback = new ThaaliFeedback();
        thaaliFeedback.setName(user.memberName);
        thaaliFeedback.setSabeel(user.sabeelNo);
        try {
            thaaliFeedback.setDate(sd.parse(date.getValue()).toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        thaaliFeedback.setMainTarkariRating(mContext.getString(map.get(mainTarkariRating.getValue())));
        thaaliFeedback.setRotiRating(mContext.getString(map.get(rotiRating.getValue())));
        thaaliFeedback.setRiceRating(mContext.getString(map.get(riceRating.getValue())));
        thaaliFeedback.setSoupRating(mContext.getString(map.get(soupRating.getValue())));
        thaaliFeedback.setRemarks(feedback.getValue() != null ? feedback.getValue() : "");
        repository.sendThaaliFeedback(thaaliFeedback)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    showLoader.setValue(false);
                    Log.d("Response", s.getData());
                    Toast.makeText(mContext, s.getData(), Toast.LENGTH_LONG).show();
                    ((ThaaliFeedbackActivity) mContext).finish();
                }, throwable -> {
                    showLoader.setValue(false);
                    Log.d("Error", throwable.getLocalizedMessage());
                    Toast.makeText(mContext, throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                });
    }


    public void showDatePicker() {
        datePickerDialog.show();
    }
}
