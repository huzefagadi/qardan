package saiflimited.com.qardan.ui.settings.subui;

import android.annotation.SuppressLint;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.repo.PasswordRepository;

public class ChangePasswordViewModel extends ViewModel {
    private final String SUCCESS_MESSAGE = "Password Successfully Changed";
    private User mUser;

    public MutableLiveData<String> getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(MutableLiveData<String> oldPassword) {
        this.oldPassword = oldPassword;
    }

    public MutableLiveData<String> getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(MutableLiveData<String> newPassword) {
        this.newPassword = newPassword;
    }

    public MutableLiveData<String> getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(MutableLiveData<String> confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

    private MutableLiveData<String> oldPassword = new MutableLiveData<>();
    private MutableLiveData<String> newPassword = new MutableLiveData<>();
    private MutableLiveData<String> confirmNewPassword = new MutableLiveData<>();
    private MutableLiveData<Boolean> showLoader = new MutableLiveData<>();
    private PasswordRepository passwordRepository = new PasswordRepository();

    public void setUser(User user) {
        this.mUser = user;
    }

    public MutableLiveData<String> getMessage() {
        return message;
    }

    private MutableLiveData<String> message = new MutableLiveData<>();


    public MutableLiveData<Boolean> getShowLoader() {
        return showLoader;
    }




    @SuppressLint("CheckResult")
    public void changePassword() {
        showLoader.setValue(true);
        passwordRepository.changePassword(mUser.itsNo, oldPassword.getValue(), newPassword.getValue())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if(SUCCESS_MESSAGE.equals(s)) {
                        oldPassword.setValue("");
                        newPassword.setValue("");
                        confirmNewPassword.setValue("");
                    }
                    showLoader.setValue(false);
                    message.setValue(s);
                }, throwable -> {
                    showLoader.setValue(false);
                    message.setValue(throwable.getLocalizedMessage());
                });
    }


    public void onButtonClick() {
        if(oldPassword.getValue().trim().length() == 0) {
            message.setValue("Old Password is required");
            return;
        }

        if(newPassword.getValue().trim().length() == 0) {
            message.setValue("New Password is required");
            return;
        }

        if(!newPassword.getValue().equals(confirmNewPassword.getValue())) {
            message.setValue("New Password and Confirm Password doesn't match");
            return;
        }
        changePassword();
    }
}
