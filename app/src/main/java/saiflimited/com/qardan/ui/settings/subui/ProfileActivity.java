package saiflimited.com.qardan.ui.settings.subui;

import android.os.Bundle;

import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.ProfileActivityBinding;

public class ProfileActivity extends AppCompatActivity {

    private ProfileViewModel mViewModel;
    private ProfileActivityBinding binding;
    private User mUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.profile_activity);
        mUser = new Gson().fromJson(getIntent().getStringExtra(Constants.INTENT_USER), User.class);
        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        mViewModel.setUser(mUser);
        binding.setViewModel(mViewModel);
    }

}
