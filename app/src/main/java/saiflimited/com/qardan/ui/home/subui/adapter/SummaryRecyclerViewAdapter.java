package saiflimited.com.qardan.ui.home.subui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.Membership;
import saiflimited.com.qardan.databinding.SummaryRowItemBinding;


public class SummaryRecyclerViewAdapter extends RecyclerView.Adapter<SummaryRecyclerViewAdapter.MyViewHolder> {

    private List<Membership> membershipList;
    private Map<Integer, String> membersCurrentBalance;
    private LayoutInflater layoutInflater;
    private SummaryRecyclerViewAdapterListener listener;

    public void loadMemberBalance(HashMap<Integer, String> integerStringHashMap) {
        membersCurrentBalance = integerStringHashMap;
        for(Integer key:integerStringHashMap.keySet()) {
            membershipList.get(key).currentBalance = integerStringHashMap.get(key);
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final SummaryRowItemBinding binding;

        public MyViewHolder(final SummaryRowItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public void loadMembers(List<Membership> memberships) {
        this.membershipList = memberships;
        notifyDataSetChanged();
    }


    public SummaryRecyclerViewAdapter(SummaryRecyclerViewAdapterListener listener) {
        this.membershipList = new ArrayList<>();
        membersCurrentBalance = new HashMap<>();
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        SummaryRowItemBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.summary_row_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.binding.setMembership(membershipList.get(position));
        if (!membersCurrentBalance.containsKey(position)) {
            listener.getCurrentBalance(position);
        }
        holder.itemView.setOnClickListener(v -> listener.onMembershipClicked(membershipList.get(position)));
    }

    @Override
    public int getItemCount() {
        return membershipList.size();
    }

    public interface SummaryRecyclerViewAdapterListener {
        void onMembershipClicked(Membership membership);
        void getCurrentBalance(int index);
    }
}