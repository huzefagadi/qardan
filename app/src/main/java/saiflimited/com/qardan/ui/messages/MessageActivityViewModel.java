package saiflimited.com.qardan.ui.messages;

import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import saiflimited.com.qardan.db.Message;

public class MessageActivityViewModel extends ViewModel {

    private MutableLiveData<Message> messageMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Message> getMessageMutableLiveData() {
        return messageMutableLiveData;
    }

    public void setMessageMutableLiveData(MutableLiveData<Message> messageMutableLiveData) {
        this.messageMutableLiveData = messageMutableLiveData;
    }
}
