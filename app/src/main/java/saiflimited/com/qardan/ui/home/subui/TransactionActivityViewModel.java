package saiflimited.com.qardan.ui.home.subui;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TransactionActivityViewModel extends ViewModel {
    private MutableLiveData<Fragment> currentFragment = new MutableLiveData<>();

    public MutableLiveData<Fragment> getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(MutableLiveData<Fragment> currentFragment) {
        this.currentFragment = currentFragment;
    }
}
