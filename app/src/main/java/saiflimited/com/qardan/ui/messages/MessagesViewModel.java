package saiflimited.com.qardan.ui.messages;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import saiflimited.com.qardan.db.Message;
import saiflimited.com.qardan.db.Repository;

public class MessagesViewModel extends ViewModel {

    public MutableLiveData<Boolean> showSpinner = new MutableLiveData<>();
    Repository repository;


    public void init(Application application) {
        repository = new Repository(application);
    }


    public List<Message> getAllMessages() {
        return repository.getAllMessages();
    }
}
