package saiflimited.com.qardan.ui.home.subui;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import saiflimited.com.qardan.beans.Membership;
import saiflimited.com.qardan.beans.Transaction;
import saiflimited.com.qardan.repo.TransactionRepository;

public class TransactionsViewModel extends ViewModel {

    public MutableLiveData<Membership> membershipMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Transaction>> transactionsLiveData = new MutableLiveData<>();
    private TransactionRepository transactionRepository = new TransactionRepository();
    public MutableLiveData<Boolean> showSpinner = new MutableLiveData<>();
    public void getMemberMobileStatement() {
        showSpinner.setValue(true);
        transactionRepository.getMemberMobileStatement(membershipMutableLiveData.getValue().memberId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactions -> {
                    showSpinner.setValue(false);
                    transactionsLiveData.setValue(transactions);
                }, throwable -> {
                    showSpinner.setValue(false);
                });
    }

    public MutableLiveData<Membership> getMembershipMutableLiveData() {
        return membershipMutableLiveData;
    }

    public void setMembershipMutableLiveData(MutableLiveData<Membership> membershipMutableLiveData) {
        this.membershipMutableLiveData = membershipMutableLiveData;
    }
}
