package saiflimited.com.qardan.ui.messages;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.databinding.MessagesFragmentBinding;
import saiflimited.com.qardan.db.Message;
import saiflimited.com.qardan.ui.home.HomeViewModel;
import saiflimited.com.qardan.ui.home.subui.adapter.MessageRecyclerViewAdapter;
import saiflimited.com.qardan.ui.home.subui.adapter.TransactionRecyclerViewAdapter;
import saiflimited.com.qardan.utils.EqualSpacingItemDecoration;

public class MessagesFragment extends Fragment implements MessageRecyclerViewAdapter.MessageRecyclerViewAdapterListener {

    private MessagesViewModel mViewModel;
    private HomeViewModel mActivityViewModel;
    private MessagesFragmentBinding binding;
    private RecyclerView mRecyclerView;

    public static MessagesFragment newInstance(String user) {
        Bundle bundle = new Bundle();
        bundle.putString("user", user);
        MessagesFragment messagesFragment = new MessagesFragment();
        messagesFragment.setArguments(bundle);
        return messagesFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.messages_fragment, container, false);
        binding.setLifecycleOwner(this);
        mRecyclerView = binding.recyclerView;
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MessagesViewModel.class);
        mViewModel.init(getActivity().getApplication());
        mActivityViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        mActivityViewModel.currentTitle.setValue("Messages");
        initRecyclerView();

    }

    private void initRecyclerView() {
        mRecyclerView = binding.recyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(mRecyclerView.getContext(),
                16, EqualSpacingItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(new MessageRecyclerViewAdapter(this));
        ((MessageRecyclerViewAdapter) mRecyclerView.getAdapter()).loadMessages(mViewModel.getAllMessages());
    }

    @Override
    public void onMessageClicked(Message message) {
        Intent intent = new Intent(getActivity(),MessageDetailActivity.class);
        intent.putExtra(Constants.INTENT_MESSAGE,new Gson().toJson(message));
        startActivity(intent);
    }
}
