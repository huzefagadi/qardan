package saiflimited.com.qardan.ui.home;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

import saiflimited.com.qardan.beans.User;

public class HomeViewModel extends ViewModel {
    public MutableLiveData<Fragment> currentFragment = new MutableLiveData<Fragment>();
    public MutableLiveData<String> currentTitle = new MutableLiveData<String>();
    public MutableLiveData<Boolean> showLoader = new MutableLiveData<>();
    FirebaseFirestore mFirestore;
    private User mUser;

    void init() {
        mFirestore = FirebaseFirestore.getInstance();
    }

    void sendUserInfoToFirestore() {
        CollectionReference users = mFirestore.collection("users");
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, Object> myMap = new Gson().fromJson(new Gson().toJson(this.mUser), type);
        users.document(mUser.sabeelNo + "_" + mUser.itsNo)
                .set(myMap)
                .addOnFailureListener(e -> Log.d("sendUserInfoToFirestore", "Failed"))
                .addOnSuccessListener(aVoid -> Log.d("sendUserInfoToFirestore", "Addition Successfuly"));
    }

    public void setUser(User user) {
        this.mUser = user;
    }
}
