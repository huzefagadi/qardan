package saiflimited.com.qardan.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.SettingsFragmentBinding;
import saiflimited.com.qardan.ui.home.HomeViewModel;
import saiflimited.com.qardan.ui.login.LoginActivity;
import saiflimited.com.qardan.ui.settings.subui.AboutUsActivity;
import saiflimited.com.qardan.ui.settings.subui.ChangePasswordActivity;
import saiflimited.com.qardan.ui.settings.subui.FeedbackActivity;
import saiflimited.com.qardan.ui.settings.subui.ProfileActivity;
import saiflimited.com.qardan.ui.settings.subui.StartStopActivity;
import saiflimited.com.qardan.ui.settings.subui.ThaaliFeedbackActivity;
import saiflimited.com.qardan.utils.EqualSpacingItemDecoration;

public class SettingsFragment extends Fragment implements SettingsRecyclerViewAdapter.SettingsRecyclerViewAdapterListener {

    private SettingsViewModel mViewModel;
    private RecyclerView mRecyclerView;
    private SettingsFragmentBinding binding;
    private List<String> settings = new ArrayList<>();
    private HomeViewModel mActivityViewModel;
    private User mUser;

    public static SettingsFragment newInstance(String user) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.INTENT_USER, user);
        SettingsFragment settingsFragment = new SettingsFragment();
        settingsFragment.setArguments(bundle);
        return settingsFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        binding = DataBindingUtil.inflate(inflater, R.layout.settings_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);
        mActivityViewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);
        mUser = new Gson().fromJson(getArguments().getString(Constants.INTENT_USER), User.class);
        mActivityViewModel.currentTitle.setValue("Settings");
        binding.setViewModel(mViewModel);
        initRecyclerView();
    }


    private void initRecyclerView() {
        settings = new ArrayList<>();
        settings.add("Profile");
        settings.add("Change Password");
        settings.add("Feedback");
        settings.add("Thaali Feedback");
        settings.add("Start/Stop Thaali");
        settings.add("About Us");
        settings.add("Log Out");
        mRecyclerView = binding.recyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(mRecyclerView.getContext(),
                16, EqualSpacingItemDecoration.VERTICAL));
        SettingsRecyclerViewAdapter settingsRecyclerViewAdapter = new SettingsRecyclerViewAdapter(this);
        settingsRecyclerViewAdapter.loadSettings(settings);
        mRecyclerView.setAdapter(settingsRecyclerViewAdapter);
    }

    @Override
    public void onSettingsClicked(int index) {
        Intent intent;
        switch (index) {
            case 0:
                intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
                break;
            case 1:
                intent = new Intent(getActivity(), ChangePasswordActivity.class);
                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
                break;
            case 2:
                intent = new Intent(getActivity(), FeedbackActivity.class);
                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
                break;
            case 3:
                intent = new Intent(getActivity(), ThaaliFeedbackActivity.class);
                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
                break;
            case 4:
                intent = new Intent(getActivity(), StartStopActivity.class);
                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
                break;
            case 5:
                intent = new Intent(getActivity(), AboutUsActivity.class);
                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
                break;
            case 6:
                intent = new Intent(getActivity(), LoginActivity.class);
                break;
            default:
                intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra(Constants.INTENT_USER, new Gson().toJson(mUser));
                break;
        }
        startActivity(intent);
        if (index == 6) {
            getActivity().finish();
        }
    }
}
