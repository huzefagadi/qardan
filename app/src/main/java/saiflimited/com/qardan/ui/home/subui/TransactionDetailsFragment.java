package saiflimited.com.qardan.ui.home.subui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.Transaction;
import saiflimited.com.qardan.databinding.TransactionDetailsFragmentBinding;

public class TransactionDetailsFragment extends Fragment {

    TransactionDetailsFragmentBinding binding;

    public static TransactionDetailsFragment newInstance() {
        return new TransactionDetailsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.transaction_details_fragment, container, false);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Transaction transaction = new Gson().fromJson(getArguments().getString(Constants.INTENT_TRANSACTION), Transaction.class);
        binding.setTransaction(transaction);
    }

}
