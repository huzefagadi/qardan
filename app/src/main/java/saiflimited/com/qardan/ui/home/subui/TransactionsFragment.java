package saiflimited.com.qardan.ui.home.subui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.beans.Membership;
import saiflimited.com.qardan.beans.Transaction;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.databinding.TransactionsFragmentBinding;
import saiflimited.com.qardan.ui.home.subui.adapter.TransactionRecyclerViewAdapter;
import saiflimited.com.qardan.utils.EqualSpacingItemDecoration;

public class TransactionsFragment extends Fragment implements TransactionRecyclerViewAdapter.TransactionRecyclerViewAdapterListener {

    private TransactionsViewModel mViewModel;
    private TransactionActivityViewModel mActivityViewModel;
    private RecyclerView mRecyclerView;
    TransactionsFragmentBinding binding;
    User mUser;
    Membership membership;

    public static final String TAG = TransactionsFragment.class.getName();

    public static TransactionsFragment newInstance() {
        return new TransactionsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.transactions_fragment, container, false);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TransactionsViewModel.class);
        mActivityViewModel = ViewModelProviders.of(getActivity()).get(TransactionActivityViewModel.class);
        initRecyclerView();
        mUser = new Gson().fromJson(getArguments().getString(Constants.INTENT_USER), User.class);
        membership = new Gson().fromJson(getArguments().getString(Constants.INTENT_MEMBER), Membership.class);
        mViewModel.membershipMutableLiveData.setValue(membership);
        binding.setViewModel(mViewModel);
        mViewModel.getMemberMobileStatement();
        mViewModel.transactionsLiveData.observe(this,
                transactions -> ((TransactionRecyclerViewAdapter) mRecyclerView.getAdapter()).loadTransactions(transactions));

        mViewModel.showSpinner.observe(this, bool -> Log.d(TAG, "onChanged: "+bool));

    }


    private void initRecyclerView() {
        mRecyclerView = binding.recyclerView;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(mRecyclerView.getContext(),
                16, EqualSpacingItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(new TransactionRecyclerViewAdapter(this));
    }

    @Override
    public void onTransactionClicked(Transaction transaction) {
        TransactionDetailsFragment transactionDetailsFragment = TransactionDetailsFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.INTENT_USER, new Gson().toJson(mUser));
        bundle.putString(Constants.INTENT_TRANSACTION, new Gson().toJson(transaction));
        transactionDetailsFragment.setArguments(bundle);
        mActivityViewModel.getCurrentFragment().setValue(transactionDetailsFragment);
    }
}
