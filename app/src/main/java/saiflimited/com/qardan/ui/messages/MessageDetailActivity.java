package saiflimited.com.qardan.ui.messages;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import saiflimited.com.qardan.Constants;
import saiflimited.com.qardan.R;
import saiflimited.com.qardan.databinding.ActivityMessageDetailBinding;
import saiflimited.com.qardan.db.Message;
import saiflimited.com.qardan.ui.home.HomeViewModel;

public class MessageDetailActivity extends AppCompatActivity {

    MessageActivityViewModel viewModel;
    ActivityMessageDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_message_detail);
        viewModel = ViewModelProviders.of(this).get(MessageActivityViewModel.class);
        binding.setViewModel(viewModel);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.message_detail));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Message message = new Gson().fromJson(getIntent().getStringExtra(Constants.INTENT_MESSAGE), Message.class);
        viewModel.getMessageMutableLiveData().setValue(message);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
