package saiflimited.com.qardan.ui.settings.subui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import saiflimited.com.qardan.beans.User;
import saiflimited.com.qardan.repo.FeedbackRepository;

public class FeedbackViewModel extends ViewModel {
    private User user;
    private MutableLiveData<String> subject = new MutableLiveData<>();
    private MutableLiveData<String> feedback = new MutableLiveData<>();
    private MutableLiveData<String> message = new MutableLiveData<>();
    private FeedbackRepository repository = new FeedbackRepository();
    private MutableLiveData<Boolean> showLoader = new MutableLiveData<>();

    public MutableLiveData<Boolean> getShowLoader() {
        return showLoader;
    }

    public MutableLiveData<String> getMessage() {
        return message;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public MutableLiveData<String> getSubject() {
        return subject;
    }

    public void setSubject(MutableLiveData<String> subject) {
        this.subject = subject;
    }

    public MutableLiveData<String> getFeedback() {
        return feedback;
    }

    public void setFeedback(MutableLiveData<String> feedback) {
        this.feedback = feedback;
    }

    public void onButtonClick() {
        if(subject.getValue().trim().isEmpty()) {
            message.setValue("Subject is Mandatory");
            return;
        }
        if(feedback.getValue().trim().isEmpty()) {
            message.setValue("Feedback is Mandatory");
            return;
        }

        showLoader.setValue(true);
        repository.sendFeedback(user.memberId, subject.getValue(), feedback.getValue())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    showLoader.setValue(false);
                    subject.setValue("");
                    feedback.setValue("");
                    message.setValue(s);
                }, throwable -> {
                    showLoader.setValue(false);
                    subject.setValue("");
                    feedback.setValue("");
                    message.setValue(throwable.getLocalizedMessage());
                });
    }
}
